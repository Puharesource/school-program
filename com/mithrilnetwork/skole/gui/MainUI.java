package com.mithrilnetwork.skole.gui;

import com.mithrilnetwork.skole.kemi.KemiUtils;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class MainUI extends JFrame
{

    private JPanel contentPane;
    private JTextField fieldInput;
    private JTextField fieldResult;

    /**
     * Create the frame.
     */
    public MainUI()
    {
        setResizable(false);
        setBounds(100, 100, 350, 132);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        final JButton buttonCalculate = new JButton("Regn");
        buttonCalculate.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent arg0) {
                fieldResult.setText(KemiUtils.calculateMolWeight(fieldInput.getText()));
            }
        });
        buttonCalculate.setBounds(10, 73, 324, 23);
        contentPane.add(buttonCalculate);

        fieldInput = new JTextField();
        fieldInput.setBounds(10, 11, 324, 20);
        contentPane.add(fieldInput);
        fieldInput.setColumns(10);

        fieldResult = new JTextField();
        fieldResult.setBounds(10, 42, 324, 20);
        contentPane.add(fieldResult);
        fieldResult.setColumns(10);
    }
}
