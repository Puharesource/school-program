package com.mithrilnetwork.skole.kemi;

public enum PeriodicTable
{
    H ("H", 1, 1.008),
    He ("He", 2, 4.003),
    Li ("Li", 3, 6.941),
    Be ("Be", 4, 9.012),
    B ("B", 5, 10.81),
    C ("C", 6, 12.01),
    N ("N", 7, 14.01),
    O ("O", 8, 16.00),
    F ("F", 9, 19.00),
    Ne ("Ne", 10, 20.18),
    Na ("Na", 11, 22.99),
    Mg ("Mg", 12, 9.012),
    Al ("Al", 13, 26.98),
    Si ("Si", 14, 28.09),
    P ("P", 15, 30.97),
    S ("S", 16, 32.07),
    Cl ("Cl", 17, 35.45),
    Ar ("Ar", 18, 39.95),
    K ("K", 19, 39.10),
    Ca ("Ca", 20, 40.08),
    Sc ("Sc", 21, 44.96),
    Ti ("Ti", 22, 47.87),
    V ("V", 23, 50.94),
    Cr ("Cr", 24, 52.00),
    Mn ("Mn", 25, 54.94),
    Fe ("Fe", 26, 55.85),
    Co ("Co", 27, 58.93),
    Ni ("Ni", 28, 58.69),
    Cu ("Cu", 29, 63.55),
    Zn ("Zn", 30, 65.38),
    Ga ("Ga", 31, 69.72),
    Ge ("Ge", 32, 72.64),
    As ("As", 33, 74.92),
    Se ("Se", 34, 78.96),
    Br ("Br", 35, 79.90),
    Kr ("Kr", 36, 83.80),
    Rb ("Rb", 37, 85.47),
    Sr ("Sr", 38, 87.62),
    Y ("Y", 39, 88.91),
    Zr ("Zr", 40, 91.22),
    Nb ("Nb", 41, 92.91),
    Mo ("Mo", 42, 95.96),
    Tc ("Tc", 43, 00.00),
    Ru ("Ru", 44, 101.1),
    Rh ("Rh", 45, 102.9),
    Pd ("Pd", 46, 106.4),
    Ag ("Ag", 47, 107.9),
    Cd ("Cd", 48, 112.4),
    In ("In", 49, 114.8),
    Sn ("Sn", 50, 118.7),
    Sb ("Sb", 51, 121.8),
    Te ("Te", 52, 127.6),
    I ("I", 53, 126.9),
    Xe ("Xe", 54, 131.3),
    Cs ("Cs", 55, 132.9),
    Ba ("Ba", 56, 137.3),
    La ("La", 57, 138.9),
    Hf ("Hf", 72, 178.5),
    Ta ("Ta", 73, 180.9),
    W ("W", 74, 183.9),
    Re ("Re", 75, 186.2),
    Os ("Os", 76, 190.2),
    Ir ("Ir", 77, 192.2),
    Pt ("Pt", 78, 195.1),
    Au ("Au", 79, 197.0),
    Hg ("Hg", 80, 200.6),
    Tl ("Tl", 81, 204.4),
    Pb ("Pb", 82, 207.2),
    Bi ("Bi", 83, 209.0),
    Po ("Po", 84, 00.00),
    At ("At", 85, 00.00),
    Rn ("Rn", 86, 00.00),
    Fr ("Fr", 87, 00.00),
    Ra ("Ra", 88, 00.00),
    Ac ("Ac", 89, 00.00),
    Rf ("Rf", 104, 00.00),
    Db ("Db", 105, 00.00),
    Sg ("Sg", 106, 00.00),
    Bh ("Bh", 107, 00.00),
    Hs ("Hs", 108, 00.00),
    Mt ("Mt", 109, 00.00),
    Ds ("Ds", 110, 00.00),
    Rg ("Rg", 111, 00.00),
    Uub ("Uub", 112, 00.00),
    Ce ("Ce", 58, 140.1),
    Pr ("Pr", 59, 140.9),
    Nd ("Nd", 60, 144.2),
    Pm ("Pm", 61, 00.00),
    Sm ("Sm", 62, 150.4),
    Eu ("Eu", 63, 151.0),
    Gd ("Gd", 64, 157.3),
    Tb ("Tb", 65, 158.9),
    Dy ("Dy", 66, 162.5),
    Ho ("Ho", 67, 164.9),
    Er ("Er", 68, 167.3),
    Tm ("Tm", 69, 168.9),
    Yb ("Yb", 70, 173.1),
    Lu ("Lu", 71, 175.0),
    Th ("Th", 90, 232.00),
    Pa ("Pa", 91, 231.00),
    U ("U", 92, 238.00),
    Np ("Np", 93, 00.00),
    Pu ("Pu", 94, 00.00),
    Am ("Am", 95, 00.00),
    Cm ("Cm", 96, 00.00),
    Bk ("Bk", 97, 00.00),
    Cf ("Cf", 98, 00.00),
    Es ("Es", 99, 00.00),
    Fm ("Fm", 100, 00.00),
    Md ("Md", 101, 00.00),
    No ("No", 102, 00.00),
    Lr ("Lr", 103, 00.00);

    private final String name;
    private final int placement;
    private final double weight;

    PeriodicTable(String name, int placement, double weight)
    {
        this.name = name;
        this.placement = placement;
        this.weight = weight;
    }

    public String getName()
    {
        return name;
    }

    public double getWeight()
    {
        return weight;
    }

    public int getPlacement()
    {
        return placement;
    }
}
