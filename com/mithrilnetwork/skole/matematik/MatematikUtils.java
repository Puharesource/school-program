package com.mithrilnetwork.skole.matematik;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

public class MatematikUtils
{
    public static String calculate(String str)
    {
        ScriptEngineManager mgr = new ScriptEngineManager();
        ScriptEngine engine = mgr.getEngineByName("JavaScript");

        try {
            return engine.eval(str).toString();
        } catch (ScriptException e) {
            return "Fejl";
        }
    }
}
