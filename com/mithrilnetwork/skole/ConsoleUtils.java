package com.mithrilnetwork.skole;

import java.util.Scanner;

public class ConsoleUtils
{
    public static String readLine()
    {
        Scanner reader = new Scanner(System.in);
        return reader.next();
    }
}
