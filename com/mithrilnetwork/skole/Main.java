package com.mithrilnetwork.skole;

import com.mithrilnetwork.skole.gui.GuiMain;
import com.mithrilnetwork.skole.gui.GuiMolCalculator;
import com.mithrilnetwork.skole.gui.MainUI;

public class Main
{
    public static void main(String[] args)
    {
        GuiMain guiMain = new GuiMain();
        guiMain.setVisible(true);
    }
}
