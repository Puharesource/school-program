package com.mithrilnetwork.skole.gui;

import javax.swing.*;
import java.awt.*;
import java.util.HashMap;
import java.util.Map;

public class GuiExtended extends JFrame
{
    protected JPanel contentPane;
    protected Map<String, JButton> buttonMap = new HashMap<>();
    protected Map<String, JPanel> panelMap = new HashMap<>();
    protected Map<String, JLabel> labelMap = new HashMap<>();

    protected void initButton(String identifier, String text, int x0, int y0, int x1, int y1)
    {
        JButton button = new JButton(text);
        button.setBounds(x0, y0, x1, y1);
        buttonMap.put(identifier, button);
        contentPane.add(button);
    }

    protected void initPanel(String identifier, Color color, int x0, int y0, int x1, int y1)
    {
        JPanel panel = new JPanel();
        panel.setBackground(color);
        panel.setBounds(x0, y0, x1, y1);
        contentPane.add(panel);
        panelMap.put(identifier, panel);
        panel.setLayout(null);
    }

    protected void initLabel(String identifier, String text, Font font, JPanel panel, int x0, int y0, int x1, int y1)
    {
        JLabel label = new JLabel(text);
        label.setBounds(x0, y0, x1, y1);
        label.setHorizontalAlignment(SwingConstants.CENTER);
        label.setFont(font);
        labelMap.put(identifier, label);
        panel.add(label);
    }
}
