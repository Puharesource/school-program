package com.mithrilnetwork.skole.kemi;

import com.mithrilnetwork.skole.matematik.MatematikUtils;

public class KemiUtils
{
    public static String getMolWeight(String atom)
    {
        for(PeriodicTable pt : PeriodicTable.values())
        {
            if(pt.getName().equalsIgnoreCase(atom))
            {
                return String.valueOf(pt.getWeight());
            }
        }
        return "00.00";
    }

    public static String calculateMolWeight(String str)
    {
        for(PeriodicTable pt : PeriodicTable.values())
        {
            if(pt.getName().length() == 3)
            {
                str = str.replace(pt.getName(), getMolWeight(pt.getName()));
                str = str.replace(pt.getName().toLowerCase(), getMolWeight(pt.getName()));
                str = str.replace(pt.getName().toUpperCase(), getMolWeight(pt.getName()));
            }
        }

        for(PeriodicTable pt : PeriodicTable.values())
        {
            if(pt.getName().length() == 2)
            {
                str = str.replace(pt.getName(), getMolWeight(pt.getName()));
                str = str.replace(pt.getName().toLowerCase(), getMolWeight(pt.getName()));
                str = str.replace(pt.getName().toUpperCase(), getMolWeight(pt.getName()));
            }
        }

        for(PeriodicTable pt : PeriodicTable.values())
        {
            if(pt.getName().length() == 1)
            {
                str = str.replace(pt.getName(), getMolWeight(pt.getName()));
                str = str.replace(pt.getName().toLowerCase(), getMolWeight(pt.getName()));
                str = str.replace(pt.getName().toUpperCase(), getMolWeight(pt.getName()));
            }
        }

        return MatematikUtils.calculate(str);
    }
}
