package com.mithrilnetwork.skole.gui;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GuiMain extends GuiExtended
{
    /**
     * Create the frame.
     */
    public GuiMain()
    {
        guiEssentials();
        setBounds(100, 100, 515, 405);
        guiObjects();
        objectEvents();
    }

    /**
     * Essentials stuff for the JFrame.
     */
    private void guiEssentials()
    {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        }
    }

    /**
     * Objects
     */
    private void guiObjects()
    {
        contentPane = new JPanel();
        contentPane.setBackground(Color.DARK_GRAY);
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        initPanel("extraPanel", Color.GRAY, 109, 0, 390, 366);
        JTextArea textArea = new JTextArea();
        textArea.setBounds(10, 54, 370, 301);
        panelMap.get("extraPanel").add(textArea);

        initLabel("title", "1xy Skole Program", new Font("Verdana", Font.BOLD, 30), panelMap.get("extraPanel"), 40, 5, 319, 38);

        initButton("molvægt", "Molvægt", 10, 11, 89, 40);
        initButton("button1", "Derp", 10, 62, 89, 40);
        initButton("button2", "Derp2", 10, 113, 89, 40);
        initButton("button3", "Derp3", 10, 164, 89, 40);
        initButton("button4", "Derp4", 10, 215, 89, 40);
        initButton("button5", "Derp5", 10, 266, 89, 40);
        initButton("button6", "Derp6", 10, 317, 89, 40);
    }

    /**
     * Events
     */
    private void objectEvents()
    {
        buttonMap.get("molvægt").addMouseListener(new MouseAdapter()
        {
            public void mouseClicked(MouseEvent args)
            {
                GuiMolCalculator calc = new GuiMolCalculator();
                calc.setVisible(true);
            }
        });
    }
}
