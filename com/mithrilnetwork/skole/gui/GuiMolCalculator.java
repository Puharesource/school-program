package com.mithrilnetwork.skole.gui;

import com.mithrilnetwork.skole.kemi.KemiUtils;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class GuiMolCalculator extends JFrame
{
    private JPanel contentPane;
    private JTextField fieldInput;
    private JTextField fieldResult;

    public GuiMolCalculator()
    {
        super("Mol Regner");
        setResizable(false);
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        }
        setBounds(100, 100, 350, 165);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        final JButton buttonCalculate = new JButton("Regn");
        buttonCalculate.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent arg0) {
                fieldResult.setText(KemiUtils.calculateMolWeight(fieldInput.getText()));
            }
        });
        buttonCalculate.setBounds(10, 102, 324, 23);
        contentPane.add(buttonCalculate);

        fieldInput = new JTextField();
        fieldInput.setBounds(10, 40, 324, 20);
        contentPane.add(fieldInput);
        fieldInput.setColumns(10);

        fieldResult = new JTextField();
        fieldResult.setBounds(10, 71, 324, 20);
        contentPane.add(fieldResult);
        fieldResult.setColumns(10);

        JLabel lblMolLommeregner = new JLabel("Mol Regner");
        lblMolLommeregner.setFont(new Font("Tahoma", Font.PLAIN, 16));
        lblMolLommeregner.setHorizontalAlignment(SwingConstants.CENTER);
        lblMolLommeregner.setBounds(10, 15, 324, 14);
        contentPane.add(lblMolLommeregner);
    }
}
